# Zbierka úloh z OOP - Kód

V tomto repozitári sa nachádza kód zo [_Zbierky úloh z OOP_](http://mm417wy.pages.kpi.fei.tuke.sk/oop-zbierka-uloh/) - modelové situácie a vyriešené úlohy. Kódy môžete voľne šíriť a používať ďalej. Zároveň slúžia ako referencia pre vaše riešenia úloh.

Pokiaľ nájdete chybu alebo nezrovnalosť, prosím, otvorte _issue_ v tomto repozitári a popíšte svoj problém.
