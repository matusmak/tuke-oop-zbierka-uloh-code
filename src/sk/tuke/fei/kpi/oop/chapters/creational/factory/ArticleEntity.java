package sk.tuke.fei.kpi.oop.chapters.creational.factory;

import java.time.LocalDate;

public class ArticleEntity {

	private final int id;
	private final boolean published;
	private final String author;
	private final String title;
	private final String content;
	private final LocalDate createdAt;

	public ArticleEntity(int id, boolean published, String author, String title, String content, LocalDate createdAt) {
		this.id = id;
		this.published = published;
		this.author = author;
		this.title = title;
		this.content = content;
		this.createdAt = createdAt;
	}

	public static ArticleEntity example() {
		return new ArticleEntity(
			-1,
			false,
			"John Doe",
			"Article title",
			"This is the content of the article",
			LocalDate.now()
		);
	}

	public int getId() {
		return id;
	}

	public boolean isPublished() {
		return published;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}
}
