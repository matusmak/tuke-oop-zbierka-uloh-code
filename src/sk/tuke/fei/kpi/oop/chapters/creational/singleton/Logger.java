package sk.tuke.fei.kpi.oop.chapters.creational.singleton;

public class Logger {

	private static Logger instance = null;

	private Logger() {
		// private constructor to mark this class singleton
	}

	public static Logger getInstance() {
		if (instance == null) {
			instance = new Logger();
		}

		return instance;
	}

	public void log(String message) {
		System.out.println(message);
	}
}
