package sk.tuke.fei.kpi.oop.chapters.creational.factory;

import sk.tuke.fei.kpi.oop.chapters.creational.builder.Car;

public class SkodaFactory implements CarFactory {

	@Override
	public Car suv() {
		return modelBuilder("Kodiaq")
			.yearOfProduction(2019)
			.dieselFuel()
			.manualGear()
			.sixGears()
			.fuelConsumption(6.0f)
			.price(34000)
			.build();
	}

	@Override
	public Car crossover() {
		throw new UnsupportedOperationException("Factory does not support making of crossover cars.");
	}

	@Override
	public Car sedan() {
		return modelBuilder("Octavia")
			.yearOfProduction(2019)
			.dieselFuel()
			.manualGear()
			.fiveGears()
			.fuelConsumption(4.5f)
			.price(22570)
			.build();
	}

	@Override
	public Car hatchback() {
		throw new UnsupportedOperationException("Factory does not support making of hatchback cars.");
	}

	@Override
	public Car coupe() {
		throw new UnsupportedOperationException("Factory does not support making of coupe cars.");
	}

	@Override
	public Car minivan() {
		throw new UnsupportedOperationException("Factory does not support making of minivan cars.");
	}

	@Override
	public Car van() {
		throw new UnsupportedOperationException("Factory does not support making of van cars.");
	}

	private Car.Builder modelBuilder(String model) {
		return new Car.Builder()
			.brand("Škoda")
			.model(model);
	}
}
