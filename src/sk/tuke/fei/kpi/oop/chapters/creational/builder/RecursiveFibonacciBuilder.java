package sk.tuke.fei.kpi.oop.chapters.creational.builder;

public class RecursiveFibonacciBuilder extends sk.tuke.fei.kpi.oop.chapters.creational.builder.AbstractFibonacciBuilder {

	@Override
	protected long compute(int index) {
		if (index <= 2) {
			return 1;
		}
		return compute(index - 1) + compute(index - 2);
	}
}
