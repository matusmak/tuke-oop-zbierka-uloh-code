package sk.tuke.fei.kpi.oop.chapters.creational.prototype;

import java.util.Objects;

public class Employee {

    private final String name;
    private final String surname;
    private final String position;
    private final int salary;

    public Employee(String name, String surname, String position, int salary) {
        this.name = Objects.requireNonNull(name, "Name cannot be null!");
        this.surname = Objects.requireNonNull(surname, "Surname cannot be null!");
        this.position = Objects.requireNonNull(position, "Position cannot be null!");
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Employee other = (Employee) o;
        return salary == other.salary &&
                Objects.equals(name, other.name) &&
                Objects.equals(surname, other.surname) &&
                Objects.equals(position, other.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, position, salary);
    }

    @Override
    public String toString() {
        return "Employee(" +
                name + ' ' + surname +
                ", " +
                position +
                ", " +
                "salary " + salary + " €" +
                ')';
    }
}
