package sk.tuke.fei.kpi.oop.chapters.creational.prototype;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

public class Company {

    private String name;
    private List<Department> departments = new ArrayList<>();

    public Company(String name) {
        this.name = Objects.requireNonNull(name, "Company name cannot be null!");
    }

    public String getName() {
        return name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public Set<Employee> getAllEmployees() {
        return departments.stream()
                .flatMap(department -> department.getMembers().stream())
                .collect(toSet());
    }

    public Optional<Department> getDepartmentOfEmployee(Employee employee) {
        return departments.stream()
                .filter(department -> department.isMember(employee))
                .findFirst();
    }

    public void employ(Employee employee, Department department) {
        Objects.requireNonNull(employee, "Employee cannot be null!");

        if (!departments.contains(department)) {
            throw new IllegalArgumentException("Unknown department!");
        }

        getDepartmentOfEmployee(employee).ifPresent(x -> {
            throw new IllegalArgumentException("Employee already employed!");
        });

        department.addMember(employee);
    }

    public void release(Employee employee) {
        getDepartmentOfEmployee(employee).ifPresent(department -> department.removeMember(employee));
    }

    public void raiseSalary(Employee employee, int salary) {
        Optional<Department> employeeDepartment = getDepartmentOfEmployee(employee);

        if (!employeeDepartment.isPresent()) {
            throw new IllegalArgumentException("Employee is not employed!");
        }

        Employee updatedEmployee = new Employee(
                employee.getName(),
                employee.getSurname(),
                employee.getPosition(),
                salary
        );

        employeeDepartment.get().removeMember(employee);
        employeeDepartment.get().addMember(updatedEmployee);
    }

    public void changePosition(Employee employee, String position) {
        Optional<Department> employeeDepartment = getDepartmentOfEmployee(employee);

        if (!employeeDepartment.isPresent()) {
            throw new IllegalArgumentException("Employee is not employed!");
        }

        Employee updatedEmployee = new Employee(
                employee.getName(),
                employee.getSurname(),
                position,
                employee.getSalary()
        );

        employeeDepartment.get().removeMember(employee);
        employeeDepartment.get().addMember(updatedEmployee);
    }

    public Company copy() {
        Company copy = new Company(name);

        copy.departments = departments.stream()
                .map(Department::copy)
                .collect(Collectors.toList());

        return copy;
    }
}
