package sk.tuke.fei.kpi.oop.chapters.creational.dependencyInjection;

import java.io.IOException;

public class WindowsTerminal implements Terminal {

	@Override
	public boolean createDirectory(String directoryPath) throws IOException {
		return runCommand("cmd", "/c", "md", toWindowsSlashes(directoryPath));
	}

	@Override
	public boolean createFile(String filePath) throws IOException {
		return runCommand("cmd", "/c", "type", "nul", ">>", toWindowsSlashes(filePath));
	}

	@Override
	public boolean removeDirectory(String directoryPath) throws IOException {
		return runCommand("cmd", "/c", "rd", "/s", "/q", toWindowsSlashes(directoryPath));
	}

	@Override
	public boolean removeFile(String filePath) throws IOException {
		return runCommand("cmd", "/c", "del", "/q", toWindowsSlashes(filePath));
	}

	private boolean runCommand(String... commandParts) throws IOException {
		Process process = new ProcessBuilder()
			.command(commandParts)
			.start();
		int exitCode;
		try {
			exitCode = process.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException("Failed to get exit code for command", e);
		}
		return exitCode == 0;
	}

	private String toWindowsSlashes(String resourceName) {
		return resourceName.replace('/', '\\');
	}
}
