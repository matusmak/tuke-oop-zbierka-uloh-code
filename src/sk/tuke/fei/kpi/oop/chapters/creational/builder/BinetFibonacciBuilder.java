package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class BinetFibonacciBuilder extends AbstractFibonacciBuilder {

	private static final BigDecimal SQUARE_ROOT_5 = new BigDecimal(5).sqrt(MathContext.DECIMAL128);
	private static final BigDecimal GOLDER_RATIO = (BigDecimal.ONE.add(SQUARE_ROOT_5))
													.divide(new BigDecimal(2), RoundingMode.HALF_UP);

	@Override
	protected long compute(int index) {
		return GOLDER_RATIO.pow(index).subtract(GOLDER_RATIO.negate().pow(-index, MathContext.DECIMAL128))
				.divide(SQUARE_ROOT_5, RoundingMode.HALF_UP)
				.setScale(0, RoundingMode.HALF_UP).longValue();
	}
}
