package sk.tuke.fei.kpi.oop.chapters.creational.dependencyInjection;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class Cache {

	private final UnixLikeTerminal terminal;

	public Cache(UnixLikeTerminal terminal) {
		this.terminal = terminal;
	}

	public boolean save(String key) throws IOException {
		String[] parts = Objects.requireNonNull(key).split("\\.");

		if (parts.length > 1) {
			String[] directoryParts = Arrays.copyOfRange(parts, 0, parts.length - 1);
			String directoryPath = String.join("/", directoryParts);
			terminal.createDirectory(directoryPath);
		}

		String cachePath = key.replace(".", "/");
		return terminal.createFile(cachePath);
	}

	public boolean delete(String key) throws IOException {
		String cachePath = key.replace('.', '/');
		return terminal.removeFile(cachePath);
	}
}
