package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractFibonacciBuilder implements FibonacciBuilder {

	private List<Long> numbers = new ArrayList<>();

	@Override
	public FibonacciBuilder add(int index) {
		numbers.add(compute(validatedIndex(index)));
		return this;
	}

	private int validatedIndex(int index) {
		if (index < 1) {
			throw new IllegalArgumentException("Index of Fibonacci's sequence must be a positive number!");
		}
		return index;
	}

	protected abstract long compute(int index);

	@Override
	public List<Long> build() {
		return Collections.unmodifiableList(new ArrayList<>(numbers));
	}
}
