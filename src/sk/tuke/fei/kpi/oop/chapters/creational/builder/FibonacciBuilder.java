package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.util.List;

public interface FibonacciBuilder {

	/**
	 * Calculates and appends next Fibonacci's number with a given index
	 *
	 * @return this instance of builder for method chaining
	 * @throws IllegalArgumentException if index is smaller than 1
	 */
	FibonacciBuilder add(int index);

	/**
	 * Creates a list with calculated Fibonacci's numbers
	 *
	 * @return new instance of immutable list of calculated numbers
	 */
	List<Long> build();
}
