package sk.tuke.fei.kpi.oop.chapters.creational.prototype;

import sk.tuke.fei.kpi.oop.chapters.creational.builder.Car;

import java.util.ArrayList;
import java.util.List;

public class CarRental {

    private List<Car> availableCars = new ArrayList<>();
    private List<Car> rentedCars = new ArrayList<>();
    private int revenue = 0;

    public CarRental() {
        // Default constructor
    }

    public CarRental(CarRental source) {
        availableCars = new ArrayList<>(source.availableCars);
        rentedCars = new ArrayList<>(source.rentedCars);
        revenue = source.revenue;
    }

    public List<Car> getAvailableCars() {
        return availableCars;
    }

    public List<Car> getRentedCars() {
        return rentedCars;
    }

    public int getRevenue() {
        return revenue;
    }

    public void addCar(Car car) {
        availableCars.add(car);
    }

    public void addCars(List<Car> car) {
        availableCars.addAll(car);
    }

    public int rentCar(Car car, int days) {
        if (!availableCars.contains(car)) {
            throw new IllegalArgumentException("Given car is not available for rental");
        }

        if (days <= 0) {
            throw new IllegalArgumentException("Cannot rent a car for less than a day!");
        }

        int rentalPrice = calculateRentalPricePerDay(car) * days;

        availableCars.remove(car);
        rentedCars.add(car);
        revenue += rentalPrice;

        return rentalPrice;
    }

    public void returnCar(Car car) {
        if (!rentedCars.contains(car)) {
            throw new IllegalArgumentException("Given car has not been rented!");
        }

        rentedCars.remove(car);
        availableCars.add(car);
    }

    public int calculateRentalPricePerDay(Car car) {
        return (int) Math.ceil(car.getPrice() / 1000.0);
    }

    public CarRental copy() {
        return new CarRental(this);
    }

    public CarRental prototype() {
        return new Prototype(this);
    }

    private static class Prototype extends CarRental {

        private Prototype(CarRental source) {
            super(source);
        }

        @Override
        public void addCar(Car car) {
            throw new UnsupportedOperationException("Cannot modify state of the prototype");
        }

        @Override
        public void addCars(List<Car> car) {
            throw new UnsupportedOperationException("Cannot modify state of the prototype");
        }

        @Override
        public int rentCar(Car car, int days) {
            throw new UnsupportedOperationException("Cannot modify state of the prototype");
        }

        @Override
        public void returnCar(Car car) {
            throw new UnsupportedOperationException("Cannot modify state of the prototype");
        }
    }
}
