package sk.tuke.fei.kpi.oop.chapters.creational.factory;

import sk.tuke.fei.kpi.oop.chapters.creational.builder.Car;

public final class FordFactory {

	private FordFactory() {
		// Private constructor to make this class static
	}

	public static Car sMax() {
		return modelBuilder("S-Max")
			.yearOfProduction(2019)
			.dieselFuel()
			.automaticGear()
			.sixGears()
			.fuelConsumption(5.8f)
			.price(39500)
			.build();
	}

	public static Car kugaAnniversary() {
		return modelBuilder("Kuga Anniversary")
			.yearOfProduction(2019)
			.gasolineFuel()
			.manualGear()
			.sixGears()
			.fuelConsumption(6.3f)
			.price(24580)
			.build();
	}

	public static Car fiesta() {
		return modelBuilder("Fiesta")
			.yearOfProduction(2019)
			.gasolineFuel()
			.manualGear()
			.sixGears()
			.fuelConsumption(5.5f)
			.price(13940)
			.build();
	}

	private static Car.Builder modelBuilder(String model) {
		return new Car.Builder()
			.brand("Ford")
			.model(model);
	}
}
