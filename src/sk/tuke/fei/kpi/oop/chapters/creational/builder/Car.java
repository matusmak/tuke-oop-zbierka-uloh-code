package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.time.Year;
import java.util.Objects;

public class Car {

	private final String brand;
	private final String model;
	private final int yearOfProduction;
	private final float powerKW;
	private final float powerHP;
	private final int engineDisplacement;
	private final FuelType fuelType;
	private final GearType gearType;
	private final int gears;
	private final float fuelConsumption;
	private final int price;

	private Car(String brand,
			   String model,
			   int yearOfProduction,
			   float powerKW,
			   float powerHP,
			   int engineDisplacement,
			   FuelType fuelType,
			   GearType gearType,
			   int gears,
			   float fuelConsumption,
			   int price) {
		this.brand = brand;
		this.model = model;
		this.yearOfProduction = yearOfProduction;
		this.powerKW = powerKW;
		this.powerHP = powerHP;
		this.engineDisplacement = engineDisplacement;
		this.fuelType = fuelType;
		this.gearType = gearType;
		this.gears = gears;
		this.fuelConsumption = fuelConsumption;
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public int getYearOfProduction() {
		return yearOfProduction;
	}

	public float getPowerKW() {
		return powerKW;
	}

	public float getPowerHP() {
		return powerHP;
	}

	public int getEngineDisplacement() {
		return engineDisplacement;
	}

	public FuelType getFuelType() {
		return fuelType;
	}

	public GearType getGearType() {
		return gearType;
	}

	public int getGears() {
		return gears;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}

	public int getPrice() {
		return price;
	}

	public enum FuelType {
		GASOLINE,
		DIESEL,
		LPG,
		ELECTRICITY
	}

	public enum GearType {
		MANUAL,
		AUTOMATIC
	}

	public static class Builder {

		private static float KW_TO_HP = 0.746f;
		private static float CI_TO_CC = 16.387064f;

		private String brand = null;
		private String model = null;
		private int yearOfProduction = Year.now().getValue();
		private float powerKW = 0;
		private float powerHP = 0;
		private int engineDisplacement = 0;
		private FuelType fuelType = FuelType.GASOLINE;
		private GearType gearType = GearType.MANUAL;
		private int gears = 5;
		private float fuelConsumption = Float.NaN;
		private int price = Integer.MIN_VALUE;

		public Builder() {
			//
		}

		public Builder ford(String model) {
			// We have used a lot of Fords in our
			// examples, so why not create a shorthand?
			return brand("Ford").model(model);
		}

		public Builder mercedes(String model) {
			// Helper methods can be as creative
			// as we want them to be
			return brand("Mercedes-Benz")
					.model(model)
					.dieselFuel()
					.automaticGear()
					.threeZeroLitersEngine();
		}

		public Builder brand(String brand) {
			this.brand = validatedBrand(brand);
			return this;
		}

		private String validatedBrand(String brand) {
			Objects.requireNonNull(brand, "Brand cannot be null!");
			if (brand.trim().isEmpty()) {
				throw new IllegalArgumentException("Brand cannot be empty!");
			}

			return brand;
		}

		public Builder model(String model) {
			this.model = validatedModel(model);
			return this;
		}

		private String validatedModel(String model) {
			Objects.requireNonNull(brand, "Model cannot be null!");
			if (model.trim().isEmpty()) {
				throw new IllegalArgumentException("Model cannot be empty!");
			}

			return brand;
		}

		public Builder yearOfProduction(int yearOfProduction) {
			this.yearOfProduction = validatedYearOfProduction(yearOfProduction);
			return this;
		}

		public Builder thisYearsModel() {
			// This might be a useless helper as current year is a
			// default value for year attribute, but we are including
			// it for the sake of demonstration
			return yearOfProduction(Year.now().getValue());
		}

		private int validatedYearOfProduction(int yearOfProduction) {
			if (yearOfProduction < 1900 || yearOfProduction > Year.now().getValue()) {
				throw new IllegalArgumentException("Year must be within a range of <1900, [current year]>!");
			}
			return yearOfProduction;
		}

		public Builder powerKW(float powerKW) {
			this.powerKW = validatedPowerKW(powerKW);
			powerHP(powerKW * KW_TO_HP);
			return this;
		}

		private float validatedPowerKW(float powerKW) {
			if (powerKW <= 0) {
				throw new IllegalArgumentException("Power in kW must be a positive number!");
			}
			return powerKW;
		}

		public Builder powerHP(float powerHP) {
			this.powerHP = validatedPowerHP(powerHP);
			powerKW(powerHP / KW_TO_HP);
			return this;
		}

		private float validatedPowerHP(float powerHP) {
			if (powerHP <= 0) {
				throw new IllegalArgumentException("Power in HP must be a positive number!");
			}
			return powerKW;
		}

		public Builder engineDisplacement(int engineDisplacement) {
			this.engineDisplacement = engineDisplacement;
			return this;
		}

		public Builder engineDisplacementInches(int engineDisplacement) {
			// In countries that still use imperial unit system,
			// engine displacement is measured in cubic inches.
			// This method encapsulates conversion to cubic centimeters
			// so that client code does not have to do it manually.
			return engineDisplacement(Math.round(engineDisplacement * CI_TO_CC));
		}

		public Builder oneSixLiterEngine() {
			return engineDisplacement(1600);
		}

		public Builder oneEightLiterEngine() {
			return engineDisplacement(1800);
		}

		public Builder twoZeroLitersEngine() {
			return engineDisplacement(2000);
		}

		public Builder threeZeroLitersEngine() {
			return engineDisplacement(3000);
		}

		private int validatedEngineDisplacement(int engineDisplacement) {
			if (engineDisplacement <= 0) {
				throw new IllegalArgumentException("Power in HP must be a positive number!");
			}
			return engineDisplacement;
		}

		public Builder fuelType(FuelType fuelType) {
			this.fuelType = validatedFuelType(fuelType);

			if (fuelType == FuelType.ELECTRICITY) {
				gearType(GearType.AUTOMATIC).gears(1);
			}

			return this;
		}

		public Builder gasolineFuel() {
			return fuelType(FuelType.GASOLINE);
		}

		public Builder dieselFuel() {
			return fuelType(FuelType.DIESEL);
		}

		public Builder diesel() {
			// When car uses diesel instead of gasoline as a fuel,
			// we are generally referring to it as "a diesel car".
			// Therefore, we are providing two different aliases
			// for `fuelType(FuelType.DIESEL)` to support both
			// natural language and keep consistency with other
			// fuel types.
			return dieselFuel();
		}

		public Builder lpgFuel() {
			return fuelType(FuelType.LPG);
		}

		public Builder electricFuel() {
			return fuelType(FuelType.ELECTRICITY);
		}

		public Builder electric() {
			// Same alias logic as with `diesel` and `dieselFuel`
			return electricFuel();
		}

		private FuelType validatedFuelType(FuelType fuelType) {
			return Objects.requireNonNull(fuelType, "Fuel type cannot be null!");
		}

		public Builder gearType(GearType gearType) {
			this.gearType = validatedGearType(gearType);
			return this;
		}

		public Builder manualGear() {
			return gearType(GearType.MANUAL);
		}

		public Builder automaticGear() {
			return gearType(GearType.AUTOMATIC);
		}

		private GearType validatedGearType(GearType gearType) {
			return Objects.requireNonNull(gearType, "Gear type cannot be null!");
		}

		public Builder gears(int gears) {
			this.gears = validatedGears(gears);
			return this;
		}

		public Builder fiveGears() {
			return gears(5);
		}

		public Builder sixGears() {
			return gears(6);
		}

		private int validatedGears(int gears) {
			if (gears < 1) {
				throw new IllegalArgumentException("Number of gears must be a positive number!");
			}
			return gears;
		}

		public Builder fuelConsumption(float fuelConsumption) {
			this.fuelConsumption = validatedFuelConsumption(fuelConsumption);
			return this;
		}

		private float validatedFuelConsumption(float fuelConsumption) {
			if (fuelConsumption <= 0) {
				throw new IllegalArgumentException("Fuel consumption must be a positive number!");
			}
			return fuelConsumption;
		}

		public Builder price(int price) {
			this.price = validatedPrice(price);
			return this;
		}

		public Builder forFree() {
			// We can be sure that this method will never be called
			// in a production code, but it serves as a good example
			return price(0);
		}

		private int validatedPrice(int price) {
			if (price < 0) {
				throw new IllegalArgumentException("Price cannot be a negative number!");
			}
			return price;
		}

		public Car build() {
			// Attributes without default values have to be validated
			// here as well to ensure they passed validation
			// process at least once
			return new Car(
					validatedBrand(brand),
					validatedModel(model),
					yearOfProduction,
					powerKW,
					powerHP,
					engineDisplacement,
					fuelType,
					gearType,
					gears,
					validatedFuelConsumption(fuelConsumption),
					validatedPrice(price)
			);
		}
	}
}
