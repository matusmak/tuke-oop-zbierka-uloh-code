package sk.tuke.fei.kpi.oop.chapters.creational.dependencyInjection;

import java.io.IOException;

public class UnixLikeTerminal implements Terminal {

	@Override
	public boolean createDirectory(String directoryPath) throws IOException {
		return runCommand("mkdir", "-p", directoryPath);
	}

	@Override
	public boolean createFile(String filePath) throws IOException {
		return runCommand("touch", filePath);
	}

	@Override
	public boolean removeDirectory(String directoryPath) throws IOException {
		return runCommand("rm", "-rf", directoryPath);
	}

	@Override
	public boolean removeFile(String filePath) throws IOException {
		return runCommand("rm", filePath);
	}

	private boolean runCommand(String... commandParts) throws IOException {
		Process process = new ProcessBuilder()
			.command(commandParts)
			.start();
		int exitCode;
		try {
			exitCode = process.waitFor();
		} catch (InterruptedException e) {
			throw new RuntimeException("Failed to get exit code for command", e);
		}
		return exitCode == 0;
	}
}
