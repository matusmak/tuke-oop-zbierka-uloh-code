package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Representation of an immutable weighted directed graph.
 *
 * @param <T> type of the nodes
 */
public class WeightedDigraph<T> {

	/**
	 * Map of all existing nodes to their neighbours
	 */
	private final Map<T, Set<Edge<T>>> nodesMap;

	/**
	 * Constructs a new instance of weighted digraph.
	 * Marked private as only {@link Builder} shall use it.
	 *
	 * @param nodesMap map of all existing nodes to their neighbours
	 */
	private WeightedDigraph(Map<T, Set<Edge<T>>> nodesMap) {
		this.nodesMap = Objects.requireNonNull(nodesMap);
	}

	/**
	 * Checks whether given node exists in digraph
	 *
	 * @param node node to be checked
	 * @return true if does exist, false otherwise
	 */
	public boolean hasNode(T node) {
		return nodesMap.containsKey(node);
	}

	/**
	 * Checks whether given node is isolated.
	 * Invalid nodes (that are not part of digraph)
	 * will return false.
	 *
	 * @param node node to be checked
	 * @return true if node exists and is isolated, false otherwise
	 */
	public boolean isNodeIsolated(T node) {
		return hasNode(node) && nodesMap.get(node).isEmpty();
	}

	/**
	 * Checks whether there is a connection from given
	 * source to given destination node
	 *
	 * @param source origin node of the edge
	 * @param destination destination node of the edge
	 * @return true if such edge exists, false otherwise
	 */
	public boolean isEdge(T source, T destination) {
		return hasNode(source)
				&& nodesMap.get(source).stream().anyMatch(edge -> Objects.equals(edge.getDestination(), destination));
	}

	/**
	 * Retrieves cost of a given edge.
	 *
	 * @param source origin node of the edge
	 * @param destination destination node of the edge
	 * @return cost of the edge, or 0 if edge or node does not exist
	 */
	public int getEdgeCost(T source, T destination) {
		if (!hasNode(source)) {
			return 0;
		}

		return nodesMap.get(source).stream()
				.filter(edge -> Objects.equals(edge.getDestination(), destination))
				.map(edge -> edge.cost)
				.findFirst()
				.orElse(0);
	}

	/**
	 * Calculates in-degree of a given node.
	 *
	 * @param node node to be checked
	 * @return in-degree of a given node, or 0 if node does not exist
	 */
	public int getNodeInDegree(T node) {
		if (!hasNode(node)) {
			return 0;
		}

		return (int) nodesMap.values().stream()
				.filter(n -> !n.equals(node))
				.filter(n -> n.stream().anyMatch(edge -> edge.destination.equals(node)))
				.count();
	}

	/**
	 * Calculates out-degree of a given node.
	 *
	 * @param node node to be checked
	 * @return out-degree of a given node, or 0 if node does not exist
	 */
	public int getNodeOutDegree(T node) {
		if (!hasNode(node)) {
			return 0;
		}

		return nodesMap.get(node).size();
	}

	/**
	 * Retrieves a set of neighbouring nodes of a given node.
	 *
	 * @param node node which neighbours will be retrieved
	 * @return set of edges of a given node, null if node does not exist
	 */
	public Set<T> getNeighbours(T node) {
		if (!hasNode(node)) {
			return null;
		}

		return nodesMap.get(node).stream()
				.map(Edge::getDestination)
				.collect(Collectors.toSet());
	}

	/**
	 * Dynamic builder of a weighted directed graph.
	 *
	 * @param <T> type of the nodes
	 */
	public static class Builder<T> {

		/**
		 * Map of all existing nodes to their neighbours
		 */
		private Map<T, Set<Edge<T>>> nodesMap = new HashMap<>();

		/**
		 * Last node set by client code. Kept as a reference
		 * to allow usage of shortcut methods for edges definition
		 */
		private T lastNode = null;

		/**
		 * Adds a new node to digraph, if not already present.
		 *
		 * @param node node to be added
		 * @return this instance of builder
		 */
		public Builder<T> node(T node) {
			return node(node, false);
		}

		/**
		 * Internal logic for adding node to the nodes map.
		 * Separated from public setter to allow tracking
		 * origin of the call.
		 *
		 * @param node node to be added
		 * @param internalCall origin of the call
		 * @return this instance of builder
		 */
		private Builder<T> node(T node, boolean internalCall) {
			Objects.requireNonNull(node, "Node cannot be null!");

			if (!nodesMap.containsKey(node)) {
				nodesMap.put(node, new HashSet<>());
			}

			// Only update last node if this method was
			// called by a client code and not internal one
			if (!internalCall) {
				lastNode = node;
			}

			return this;
		}

		/**
		 * Define edge for the last defined node.
		 * Cost of the edge will be 0.
		 *
		 * @param destination destination node of the edge
		 * @return this instance of builder
		 */
		public Builder<T> edge(T destination) {
			return edge(destination, 0);
		}

		/**
		 * Define edge for the last defined node with a given cost.
		 *
		 * @param destination destination node of the edge
		 * @param cost cost of this edge
		 * @return this instance of builder
		 */
		public Builder<T> edge(T destination, int cost) {
			return edge(
					Objects.requireNonNull(lastNode, "Cannot call plain `edge` method when `node` was not called!"),
					destination,
					cost
			);
		}

		/**
		 * Define edge from the origin to the destination node.
		 * Cost of the edge will be 0.
		 *
		 * @param node origin node of the edge
		 * @param destination destination node of the edge
		 * @return this instance of builder
		 */
		public Builder<T> edge(T node, T destination) {
			return edge(node, destination, 0);
		}

		/**
		 * Define edge from the origin to the destination node with a given cost.
		 *
		 * @param node origin node of the edge
		 * @param destination destination node of the edge
		 * @param cost cost of the edge
		 * @return this instance of builder
		 */
		public Builder<T> edge(T node, T destination, int cost) {
			// Ensure nodes are registered in nodes map
			// This is just a convenience for client developer
			node(node, true).node(destination, true);

			// Create and add an edge
			// Ensure that any previous similar edge will be removed,
			// so that the latest value will be used
			Edge<T> edge = new Edge<>(destination, cost);
			Set<Edge<T>> nodeEdges = nodesMap.get(node);
			nodeEdges.remove(edge);
			nodeEdges.add(edge);

			return this;
		}

		/**
		 * Constructs weighted digraph based on defined nodes and edges
		 *
		 * @return a new instance of immutable WeightedDigraph
		 */
		public WeightedDigraph<T> build() {
			return new WeightedDigraph<>(
					Collections.unmodifiableMap(new HashMap<>(nodesMap))
			);
		}
	}

	/**
	 * Internal representation of the edge
	 *
	 * @param <T> type of the nodes
	 */
	private static class Edge<T> {

		private final T destination;
		private final int cost;

		private Edge(T destination, int cost) {
			this.destination = destination;
			this.cost = cost;
		}

		public T getDestination() {
			return destination;
		}

		public int getCost() {
			return cost;
		}

		@Override
		public int hashCode() {
			// Custom hash generation for edge class.
			// Only including destination in hash generation
			// means that two edges with the same destination
			// but different costs would evaluate as the same
			// element in the HashSet.
			return Objects.hash(destination);
		}
	}
}
