package sk.tuke.fei.kpi.oop.chapters.creational.builder;

public class LoopFibonacciBuilder extends sk.tuke.fei.kpi.oop.chapters.creational.builder.AbstractFibonacciBuilder {

	@Override
	protected long compute(int index) {
		if (index <= 2) {
			return 1;
		}

		long previousValue = 1;
		long finalValue = 2;

		for (int i = 3; i < index; i++) {
			long newFinalValue = previousValue + finalValue;
			previousValue = finalValue;
			finalValue = newFinalValue;
		}

		return finalValue;
	}
}
