package sk.tuke.fei.kpi.oop.chapters.creational.factory;

public class ArticleDto {

	private final String id;
	private final String author;
	private final String header;
	private final String body;

	public ArticleDto(String id, String author, String header, String body) {
		this.id = id;
		this.author = author;
		this.header = header;
		this.body = body;
	}

	public static ArticleDto fromEntity(ArticleEntity entity) {
		return new ArticleDto(
			Integer.toString(entity.getId()),
			entity.getAuthor(),
			entity.getTitle(),
			entity.getContent()
		);
	}

	public String getId() {
		return id;
	}

	public String getAuthor() {
		return author;
	}

	public String getHeader() {
		return header;
	}

	public String getBody() {
		return body;
	}
}
