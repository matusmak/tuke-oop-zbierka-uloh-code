package sk.tuke.fei.kpi.oop.chapters.creational.singleton;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FileLogger {

	private static final String DEFAULT_LOGGER_FILENAME = "app.log";
	private static final Map<String, FileLogger> instances = new HashMap<>();

	private final PrintWriter printWriter;

	private FileLogger(String filename) throws IOException {
		FileWriter fileWriter = new FileWriter(filename, true);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		printWriter = new PrintWriter(bufferedWriter);
	}

	public static FileLogger getDefaultInstance() {
		return getInstance(DEFAULT_LOGGER_FILENAME);
	}

	public static FileLogger getInstance(String filename) {
		Objects.requireNonNull(filename);

		instances.computeIfAbsent(filename, loggerFilename -> {
			try {
				return new FileLogger(loggerFilename);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		});

		return instances.get(filename);
	}

	public void log(String message) {
		printWriter.println(message);
	}
}
