package sk.tuke.fei.kpi.oop.chapters.creational.dependencyInjection;

import java.io.IOException;

public interface Terminal {

	boolean createDirectory(String directoryPath) throws IOException;

	boolean createFile(String filePath) throws IOException;

	boolean removeDirectory(String directoryPath) throws IOException;

	boolean removeFile(String filePath) throws IOException;
}
