package sk.tuke.fei.kpi.oop.chapters.creational.prototype;

import sk.tuke.fei.kpi.oop.chapters.creational.factory.CarFactory;
import sk.tuke.fei.kpi.oop.chapters.creational.factory.FordFactory;
import sk.tuke.fei.kpi.oop.chapters.creational.factory.SkodaFactory;

public class CarRentalFactory {

    private static final CarRental GENERAL_CAR_RENTAL_PROTOTYPE = createGeneralCarRental();
    private static final CarRental FORD_CAR_RENTAL_PROTOTYPE = createFordCarRental();
    private static final CarRental SKODA_CAR_RENTAL_PROTOTYPE = createSkodaCarRental();


    public static CarRental general() {
        return GENERAL_CAR_RENTAL_PROTOTYPE.copy();
    }

    public static CarRental ford() {
        return FORD_CAR_RENTAL_PROTOTYPE.copy();
    }

    public static CarRental skoda() {
        return SKODA_CAR_RENTAL_PROTOTYPE.copy();
    }

    private static CarRental createGeneralCarRental() {
        CarFactory skodaFactory = new SkodaFactory();
        CarRental rental = new CarRental();

        rental.addCar(FordFactory.sMax());
        rental.addCar(FordFactory.fiesta());
        rental.addCar(skodaFactory.suv());
        rental.addCar(skodaFactory.sedan());

        return rental;
    }

    private static CarRental createFordCarRental() {
        CarRental rental = new CarRental();

        rental.addCar(FordFactory.sMax());
        rental.addCar(FordFactory.fiesta());

        return rental;
    }

    private static CarRental createSkodaCarRental() {
        CarFactory skodaFactory = new SkodaFactory();
        CarRental rental = new CarRental();

        rental.addCar(skodaFactory.suv());
        rental.addCar(skodaFactory.sedan());

        return rental;
    }
}
