package sk.tuke.fei.kpi.oop.chapters.creational.builder;

import java.util.List;

public class FirstTenFibonacciBuildDirector {

	private final FibonacciBuilder builder;
	private boolean setup = false;

	public FirstTenFibonacciBuildDirector(FibonacciBuilder builder) {
		this.builder = builder;
	}

	public List<Long> construct() {
		setupBuilder();
		return builder.build();
	}

	private void setupBuilder() {
		if (setup) {
			return;
		}

		for (int i = 1; i <= 10; i++) {
			builder.add(i);
		}

		setup = true;
	}
}
