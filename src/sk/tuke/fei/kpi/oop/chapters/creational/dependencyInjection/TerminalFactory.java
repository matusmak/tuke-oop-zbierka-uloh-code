package sk.tuke.fei.kpi.oop.chapters.creational.dependencyInjection;

import java.util.Objects;

public class TerminalFactory {

	private TerminalFactory() {
		// Making this class static
	}

	public static Terminal makeTerminal() {
		return makeTerminal(System.getProperty("os.name"));
	}

	public static Terminal makeTerminal(String os) {
		Objects.requireNonNull(os);

		if (isWindows(os)) {
			return new WindowsTerminal();
		} else if (isUnixLike(os)) {
			return new UnixLikeTerminal();
		}

		throw new IllegalArgumentException("Invalid os: " + os);
	}

	private static boolean isUnixLike(String os) {
		String osLowercase = os.toLowerCase();
		return osLowercase.contains("nix") || osLowercase.contains("nux") || osLowercase.contains("aix");
	}

	private static boolean isWindows(String os) {
		return os.toLowerCase().contains("windows");
	}
}
