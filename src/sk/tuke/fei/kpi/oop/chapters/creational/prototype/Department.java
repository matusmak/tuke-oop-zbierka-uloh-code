package sk.tuke.fei.kpi.oop.chapters.creational.prototype;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Department {

    private String name;
    private Set<Employee> members = new HashSet<>();

    public Department(String name) {
        this.name = Objects.requireNonNull(name, "Department name cannot be null!");
    }

    public String getName() {
        return name;
    }

    public Set<Employee> getMembers() {
        return members;
    }

    public void addMember(Employee employee) {
        members.add(employee);
    }

    public void removeMember(Employee employee) {
        members.remove(employee);
    }

    public boolean isMember(Employee employee) {
        return members.contains(employee);
    }

    public Department copy() {
        Department copy = new Department(name);

        copy.members = new HashSet<>(members);

        return copy;
    }
}
