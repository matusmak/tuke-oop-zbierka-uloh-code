package sk.tuke.fei.kpi.oop.chapters.creational.factory;

import sk.tuke.fei.kpi.oop.chapters.creational.builder.Car;

public interface CarFactory {

	Car suv();

	Car crossover();

	Car sedan();

	Car hatchback();

	Car coupe();

	Car minivan();

	Car van();
}
