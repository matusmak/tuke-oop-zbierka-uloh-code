package sk.tuke.fei.kpi.oop.chapters.creational.factory;

import sk.tuke.fei.kpi.oop.chapters.creational.builder.Car;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class BarcodeCarFactory {

	private static final CarFactory SKODA_FACTORY = new SkodaFactory();

	private static final Map<String, Supplier<Car>> CAR_SUPPLIERS = new HashMap<String, Supplier<Car>>() {
		{
			put("FRD-19-SMX-2D", FordFactory::sMax);
			put("FRD-19-KGANNVSR-1.5G", FordFactory::kugaAnniversary);
			put("FRD-19-FST-1G", FordFactory::fiesta);

			put("SKD-19-KDQ-2D", SKODA_FACTORY::suv);
			put("SKD-19-OCTV-1.6D", SKODA_FACTORY::sedan);
		}
	};

	public static Optional<Car> make(String barcode) {
		Objects.requireNonNull(barcode, "Barcode cannot be null!");

		return CAR_SUPPLIERS.containsKey(barcode)
			? Optional.of(CAR_SUPPLIERS.get(barcode).get())
			: Optional.empty();
	}
}
