package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

public class ConsoleLoggerWriter implements LoggerWriter {

    @Override
    public void write(String message) {
        System.out.println(message);
    }
}
