package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.text.DecimalFormat;
import java.util.function.Function;

public class LoggerMiddleware implements Middleware {

	private final DecimalFormat formatter = new DecimalFormat("0.0#####");

	@Override
	public Response handle(Request request, Function<Request, Response> next) {
		System.out.println("Request for " + request.getMethod().toString() + " " + request.getUri());

		long start = System.nanoTime();
		Response response = next.apply(request);
		long end = System.nanoTime();

		double diffMillis = (end - start) / 1000000D;
		String diff = formatter.format(diffMillis);

		System.out.println("Request finished. Time " + diff + " milliseconds.");

		return response;
	}
}
