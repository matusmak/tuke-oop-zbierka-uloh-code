package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.function.Function;

public interface Middleware {

	Response handle(Request request, Function<Request, Response> next);
}
