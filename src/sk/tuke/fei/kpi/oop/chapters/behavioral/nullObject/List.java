package sk.tuke.fei.kpi.oop.chapters.behavioral.nullObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class List<T> {

	private static final List<?> NIL = new Empty<>();

	public abstract T head();
	public abstract List<T> tail();
	public abstract boolean isEmpty();

	public static <T> List<T> nil() {
		//noinspection unchecked
		return (List<T>) NIL;
	}

	/**
	 * Creates a new list from given elements in specified order.
	 *
	 * Example:
	 * List.of(42, 314) == List(42, 314)
	 *
	 * @param elements elements to be wrapped into list
	 * @return new instance of a functional list
	 */
	public static <T> List<T> of(T... elements) {
		if (elements.length == 0) {
			return nil();
		}

		java.util.List<T> input = Arrays.asList(elements);
		Collections.reverse(input);

		List<T> list = nil();

		for (T element : input) {
			list = list.cons(element);
		}

		return list;
	}

	/**
	 * Prepends given element in front of this list
	 *
	 * Example:
	 * List.of(42).cons(314) == List(314, 42)
	 *
	 * @param element element to be prepended
	 * @return new instance of a functional list
	 */
	public List<T> cons(T element) {
		return new NonEmpty<>(element, this);
	}

	/**
	 * Prepends all elements in front of this list
	 *
	 * Example:
	 * List.of(42, 314).cons(List.of(161, 271)) == List(161, 271, 42, 314)
	 *
	 * @param that list to be prepended
	 * @return new instance of a functional list
	 */
	public List<T> cons(List<T> that) {
		List<T> reversed = reverse();
		List<T> merged = that;

		while (!reversed.isEmpty()) {
			merged = merged.cons(reversed.head());
			reversed = reversed.tail();
		}

		return merged;
	}

	/**
	 * Creates new list from the same elements with reversed order
	 *
	 * Example:
	 * List.of(42, 314).reverse() == List(314, 42)
	 *
	 * @return new instance of a functional list
	 */
	public List<T> reverse() {
		List<T> reversed = nil();
		List<T> pivot = this;

		while (!pivot.isEmpty()) {
			reversed = reversed.cons(pivot.head());
			pivot = pivot.tail();
		}

		return reversed;
	}

	/**
	 * Applies given callback to all elements.
	 *
	 * Example:
	 * List.of(42, 314).forEach(number -> System.out.println(number))
	 * > 42
	 * > 314
	 *
	 * @param consumer callback that will consume elements from this list
	 */
	public void forEach(Consumer<T> consumer) {
		List<T> pivot = this;

		while (!pivot.isEmpty()) {
			consumer.accept(pivot.head());
			pivot = pivot.tail();
		}
	}

	/**
	 * Maps this list to a new one using given function
	 *
	 * Example:
	 * List.of(42, 314).map(number -> number * 2) == List(84, 628)
	 *
	 * @param function function that will be used to map elements from this list to the new list
	 * @return new instance of a functional list
	 */
	public <R> List<R> map(Function<T, R> function) {
		List<T> pivot = this;
		List<R> mapped = nil();

		while (!pivot.isEmpty()) {
			mapped.cons(function.apply(pivot.head()));
			pivot = pivot.tail();
		}

		return mapped.reverse();
	}

	@Override
	public String toString() {
		List<T> pivot = this;

		StringBuilder builder = new StringBuilder("List(");

		while (!pivot.isEmpty()) {
			builder.append(pivot.head());

			if (!pivot.tail().isEmpty()) {
				builder.append(", ");
			}

			pivot = pivot.tail();
		}

		return builder.append(')').toString();
	}

	@Override
	public int hashCode() {
		if (isEmpty()) {
			return super.hashCode();
		} else if (tail().isEmpty()) {
			return Objects.hash(getClass(), head());
		} else {
			return Objects.hash(getClass(), head(), tail());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		List<T> thisPivot = this;
		List thatPivot = (List) o;

		while (!thisPivot.isEmpty() && !thatPivot.isEmpty()) {
			if (thisPivot.isEmpty() != thatPivot.isEmpty()) {
				return false;
			}

			if (!Objects.equals(thisPivot.head(), thatPivot.head())) {
				return false;
			}

			thisPivot = thisPivot.tail();
			thatPivot = thatPivot.tail();
		}

		return true;
	}

	private static class NonEmpty<T> extends List<T> {

		private final T head;
		private final List<T> tail;

		private NonEmpty(T head, List<T> tail) {
			this.head = head;
			this.tail = Objects.requireNonNull(tail, "Tail cannot be null!");
		}

		@Override
		public T head() {
			return head;
		}

		@Override
		public List<T> tail() {
			return tail;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}

	private static class Empty<T> extends List<T> {

		@Override
		public T head() {
			throw new IllegalStateException("Head on empty");
		}

		@Override
		public List<T> tail() {
			throw new IllegalStateException("Tail on empty");
		}

		@Override
		public boolean isEmpty() {
			return true;
		}
	}
}
