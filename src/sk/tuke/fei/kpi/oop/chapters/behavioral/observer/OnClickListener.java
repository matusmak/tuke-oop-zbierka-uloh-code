package sk.tuke.fei.kpi.oop.chapters.behavioral.observer;

@FunctionalInterface
public interface OnClickListener<V extends View> {

    void onClick(V view);

    default void onDoubleClick(V view) { }

    default void onLongClick(V view) { }
}
