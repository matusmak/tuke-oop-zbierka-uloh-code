package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.*;

public class MiddlewareExample {

	/**
	 * Scenario: Attempt to log in to the system.
	 * Expected output (exact time will vary):
	 *
	 * Request for POST auth/login
	 * Request finished. Time 0.118638 milliseconds.
	 * Locale = en; Body = user=example&password=password
	 */
	public static void example1() {
		Request request = new Request(
			Request.Method.POST,
			"auth/login",
			"user=example&password=password",
			null,
			Collections.emptyMap()
		);

		Response response = runWithMiddlewares(request, middlewares());

		System.out.println(response.getBody());
	}

	/**
	 * Scenario: Attempt to access page without being authenticated
	 * Expected output (exact time will vary):
	 *
	 * Request for GET dashboard
	 * Request finished. Time 0.035112 milliseconds.
	 * Not authenticated!
	 */
	public static void example2() {
		Request request = new Request(
			Request.Method.GET,
			"dashboard",
			null,
			null,
			Collections.emptyMap()
		);

		Response response = runWithMiddlewares(request, middlewares());

		System.out.println(response.getBody());
	}

	/**
	 * Scenario: Attempt to access page with incorrect authentication token
	 * Expected output (exact time will vary):
	 *
	 * Request for GET dashboard
	 * Request finished. Time 0.002819 milliseconds.
	 * Not authenticated!
	 */
	public static void example3() {
		Map<String, String> headers = new HashMap<String, String>(){
			{
				put("Authenticate", "BD879");
			}
		};

		Request request = new Request(
			Request.Method.GET,
			"dashboard",
			null,
			null,
			headers
		);

		Response response = runWithMiddlewares(request, middlewares());

		System.out.println(response.getBody());
	}

	/**
	 * Scenario: Attempt to access page with correct authentication token and custom locale preference
	 * Expected output (exact time will vary):
	 *
	 * Request for GET dashboard
	 * Request finished. Time 0.010345 milliseconds.
	 * Locale = sk; Body = Body with spaces
	 */
	public static void example4() {
		Map<String, String> headers = new HashMap<String, String>(){
			{
				put("Authenticate", "TKN345");
				put("Locale", "sk");
			}
		};

		Request request = new Request(
			Request.Method.GET,
			"dashboard",
			"      Body with spaces       ",
			null,
			headers
		);

		Response response = runWithMiddlewares(request, middlewares());

		System.out.println(response.getBody());
	}

	private static List<Middleware> middlewares() {
		return Arrays.asList(
			new LoggerMiddleware(),
			new AuthMiddleware(),
			new LocaleMiddleware(),
			new TrimMiddleware()
		);
	}

	private static Response runWithMiddlewares(Request request, List<Middleware> middlewares) {
		// This is just a preparation for the first call of recursive `nextHandler` method
		Middleware currentMiddleware = middlewares.isEmpty() ? null : middlewares.get(0);
		return nextHandler(request, 0, middlewares, currentMiddleware);
	}

	private static Response nextHandler(Request request, int currentIndex,
										List<Middleware> allMiddlewares, Middleware currentMiddleware) {
		// The last middleware should finish by triggering
		// controller instead of next middleware
		if (currentIndex + 1 >= allMiddlewares.size()) {
			return currentMiddleware.handle(
				request,
				currentRequest -> new PageController().getPage(request)
			);
		}

		// If we are in the middle of the chain, then middleware
		// should be able to call the next one in the chain
		int nextIndex = currentIndex + 1;
		Middleware nextMiddleware = allMiddlewares.get(nextIndex);
		return currentMiddleware.handle(
			request,
			// The reason we are accepting different request than the one
			// received from `runWIthMiddlewares` is that we want to allow
			// middlewares to not only change, but also completely replace
			// requests if they feel it's necessary to do so
			currentRequest -> nextHandler(currentRequest, nextIndex, allMiddlewares, nextMiddleware)
		);
	}
}
