package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.time.LocalDateTime;

public class Logger {

    private final LoggerWriter writer;
    private boolean showDebugMessages = true;

    public Logger(LoggerWriter writer) {
        this.writer = writer;
    }

    public void debug(String message) {
        print(true, message);
    }

    public void error(String message) {
        print(false, message);
    }

    public void setShowDebugMessages(boolean showDebugMessages) {
        this.showDebugMessages = showDebugMessages;
    }

    public boolean shouldShowDebugMessages() {
        return showDebugMessages;
    }

    private void print(boolean isDebug, String message) {
        // Do not print debug messages if option is disabled
        if (isDebug && !showDebugMessages) {
            return;
        }

        String formattedMessage = String.format("[%s][%s] %s",
                LocalDateTime.now(),
                isDebug ? "DEBUG" : "ERROR",
                message
        );

        writer.write(formattedMessage);
    }
}
