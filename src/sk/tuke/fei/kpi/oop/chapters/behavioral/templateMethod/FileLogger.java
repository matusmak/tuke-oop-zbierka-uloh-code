package sk.tuke.fei.kpi.oop.chapters.behavioral.templateMethod;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileLogger extends AbstractLogger {

	private static final String FILENAME = "application.log";

	private final PrintWriter printWriter;

	public FileLogger() throws IOException {
		FileWriter fileWriter = new FileWriter(FILENAME, true);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		printWriter = new PrintWriter(bufferedWriter);
	}

	@Override
	protected void write(String message) {
		printWriter.println(message);
	}
}
