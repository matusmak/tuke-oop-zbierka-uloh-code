package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.function.Function;

public class TrimMiddleware implements Middleware {

	@Override
	public Response handle(Request request, Function<Request, Response> next) {
		if (request.getBody() != null) {
			request.setBody(request.getBody().trim());
		}

		return next.apply(request);
	}
}
