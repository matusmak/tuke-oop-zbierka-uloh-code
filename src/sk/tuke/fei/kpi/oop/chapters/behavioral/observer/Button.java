package sk.tuke.fei.kpi.oop.chapters.behavioral.observer;

public class Button extends View<Button> {

    public Button(View parent, String name) {
        super(parent, name);
    }

    public void performClick() {
        getOnClickListeners().forEach(listener -> listener.onClick(this));
    }

    public void performDoubleClick() {
        getOnClickListeners().forEach(listener -> listener.onDoubleClick(this));
    }

    public void performLongClick() {
        getOnClickListeners().forEach(listener -> listener.onLongClick(this));
    }
}
