package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

public class PageController {

	public Response getPage(Request request) {
		String body = "Locale = " + request.getLocale() + "; Body = " + request.getBody();
		return new Response(body);
	}
}
