package sk.tuke.fei.kpi.oop.chapters.behavioral.templateMethod;

public interface Logger {

	void debug(String message);

	void error(String message);

	void setShowDebugMessages(boolean showDebugMessages);

	boolean shouldShowDebugMessages();
}
