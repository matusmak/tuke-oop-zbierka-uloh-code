package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.util.Objects;

public class Apple {

    private final int weight;
    private final Cultivar cultivar;
    private final QualityClass qualityClass;

    public Apple(int weight, Cultivar cultivar, QualityClass qualityClass) {
        this.weight = weight;
        this.cultivar = Objects.requireNonNull(cultivar, "Cultivar cannot be null!");
        this.qualityClass = Objects.requireNonNull(qualityClass, "Quality class cannot be null!");
    }

    public int getWeight() {
        return weight;
    }

    public Cultivar getCultivar() {
        return cultivar;
    }

    public QualityClass getQualityClass() {
        return qualityClass;
    }

    @Override
    public String toString() {
        return "Apple("
                + weight + "g, "
                + cultivar + " cultivar, "
                + qualityClass + " quality)";
    }

    /**
     * Selected cultivars from https://en.wikipedia.org/wiki/Apple#Cultivars
     */
    public enum Cultivar {
        ALICE,
        AMBROSIA,
        GOLDEN_DELICIOUS,
        GRANNY_SMITH,
        MCINTOSH,
    }

    /**
     * Qualities based on https://www.applesfromeurope.eu/for-professionals/commercial-quality-of-apples
     */
    public enum QualityClass {
        EXTRA,
        I,
        II
    }
}
