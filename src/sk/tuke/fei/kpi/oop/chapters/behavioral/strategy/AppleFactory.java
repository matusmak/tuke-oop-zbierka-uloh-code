package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.util.stream.Stream;

public class AppleFactory {

    private final Generator generator;

    public AppleFactory(Generator generator) {
        this.generator = generator;
    }

    public Apple make() {
        return Stream.generate(this::generate)
                .filter(generator::isValid)
                .findFirst()
                .get();
    }

    private Apple generate() {
        return new Apple(
                generator.generateWeight(),
                generator.generateCultivar(),
                generator.generateQualityClass()
        );
    }

    public interface Generator {

        int generateWeight();

        Apple.Cultivar generateCultivar();

        Apple.QualityClass generateQualityClass();

        boolean isValid(Apple apple);
    }
}
