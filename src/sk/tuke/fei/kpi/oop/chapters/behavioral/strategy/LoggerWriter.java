package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

public interface LoggerWriter {

    void write(String message);
}
