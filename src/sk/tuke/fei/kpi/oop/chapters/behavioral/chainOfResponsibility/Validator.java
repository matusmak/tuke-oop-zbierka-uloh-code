package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.*;
import java.util.function.Predicate;

public class Validator<T> {

    private static final Validator<?> NULL_VALIDATOR = new NullValidator<>();

    private final Predicate<T> predicate;
    private final Validator<T> next;

    /**
     * Special constructor used by NullValidator
     */
    private Validator() {
        this.predicate = null;
        this.next = null;
    }

    private Validator(Predicate<T> predicate, Validator<T> next) {
        this.predicate = Objects.requireNonNull(predicate);
        this.next = Objects.requireNonNull(next);
    }

    @SuppressWarnings("unchecked")
    public static <T> Validator<T> rule(Predicate<T> validator) {
        return new Validator<>(validator, (Validator<T>) NULL_VALIDATOR);
    }

    public Validator<T> and(Predicate<T> validator) {
        return new Validator<>(validator, this);
    }

    public boolean validate(T element) {
        if (!predicate.test(element)) {
            return false;
        }

        return next.validate(element);
    }

    private static class NullValidator<T> extends Validator<T> {

        private NullValidator() {
            super();
        }

        @Override
        public boolean validate(T element) {
            return true;
        }
    }
}
