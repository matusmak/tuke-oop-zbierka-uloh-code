package sk.tuke.fei.kpi.oop.chapters.behavioral.observer;

public class Label extends View<Label> {

    private final String label;

    public Label(View parent, String name, String label) {
        super(parent, name);
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
