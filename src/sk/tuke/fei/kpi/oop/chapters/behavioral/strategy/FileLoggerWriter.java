package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileLoggerWriter implements LoggerWriter {

    private static final String FILENAME = "application.log";

    private final PrintWriter printWriter;

    public FileLoggerWriter() throws IOException {
        FileWriter fileWriter = new FileWriter(FILENAME, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        printWriter = new PrintWriter(bufferedWriter);
    }

    @Override
    public void write(String message) {
        printWriter.println(message);
    }
}
