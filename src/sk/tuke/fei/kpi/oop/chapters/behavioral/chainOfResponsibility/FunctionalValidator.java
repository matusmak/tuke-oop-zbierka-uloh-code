package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.Objects;

@FunctionalInterface
public interface FunctionalValidator<T> {

    boolean validate(T element);

    static <T> FunctionalValidator<T> rule(FunctionalValidator<T> validator) {
        Objects.requireNonNull(validator);
        return validator;
    }

    default FunctionalValidator<T> and(FunctionalValidator<T> validator) {
        Objects.requireNonNull(validator);
        return element -> validate(element) && validator.validate(element);
    }
}
