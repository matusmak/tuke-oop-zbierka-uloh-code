package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.util.Random;
public class RandomAppleGenerator implements AppleFactory.Generator {

    private final Random random = new Random();

    @Override
    public int generateWeight() {
        return random.nextInt(191) + 50;
    }

    @Override
    public Apple.Cultivar generateCultivar() {
        return Apple.Cultivar.values()[random.nextInt(Apple.Cultivar.values().length)];
    }

    @Override
    public Apple.QualityClass generateQualityClass() {
        return Apple.QualityClass.values()[random.nextInt(Apple.QualityClass.values().length)];
    }

    @Override
    public boolean isValid(Apple apple) {
        return true;
    }
}

