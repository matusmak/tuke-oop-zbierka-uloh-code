package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.Map;

public class Request {

    private Method method;
    private String uri;
    private String body;
    private String locale;
    private Map<String, String> headers;

    public Request(Method method, String uri, String body, String locale, Map<String, String> headers) {
        this.method = method;
        this.uri = uri;
        this.body = body;
        this.locale = locale;
        this.headers = headers;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public enum Method {
        GET, POST, PUT, DELETE, PATCH,
        HEAD, TRACE, OPTIONS, CONNECT
    }
}
