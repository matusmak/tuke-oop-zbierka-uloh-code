package sk.tuke.fei.kpi.oop.chapters.behavioral.templateMethod;

import java.time.LocalDateTime;

public abstract class AbstractLogger implements Logger {

	private boolean showDebugMessages = true;

	@Override
	public void debug(String message) {
		print(true, message);
	}

	@Override
	public void error(String message) {
		print(false, message);
	}

	@Override
	public void setShowDebugMessages(boolean showDebugMessages) {
		this.showDebugMessages = showDebugMessages;
	}

	@Override
	public boolean shouldShowDebugMessages() {
		return showDebugMessages;
	}

	private void print(boolean isDebug, String message) {
		// Do not print debug messages if option is disabled
		if (isDebug && !showDebugMessages) {
			return;
		}

		String formattedMessage = String.format("[%s][%s] %s",
			LocalDateTime.now(),
			isDebug ? "DEBUG" : "ERROR",
			message
		);

		write(formattedMessage);
	}

	protected abstract void write(String message);
}
