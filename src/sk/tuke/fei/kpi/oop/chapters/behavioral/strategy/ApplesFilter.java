package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ApplesFilter {

    public static List<Apple> filter(List<Apple> apple, Predicate<Apple> condition) {
        return apple.stream()
                .filter(condition)
                .collect(Collectors.toList());
    }

    public static List<Apple> filterHeavierThan(List<Apple> apples, int weight) {
        return filter(apples, heavierThan(weight).and(quality(null)));
    }

    public static List<Apple> filterQuality(List<Apple> apples, Apple.QualityClass quality) {
        return filter(apples, quality(quality));
    }

    public static List<Apple> filterCultivar(List<Apple> apples, Apple.Cultivar cultivar) {
        return filter(apples, cultivar(cultivar));
    }

    public static Predicate<Apple> heavierThan(int weight) {
        return apple -> apple.getWeight() > weight;
    }

    public static Predicate<Apple> quality(Apple.QualityClass quality) {
        return apple -> apple.getQualityClass() == quality;
    }

    public static Predicate<Apple> cultivar(Apple.Cultivar cultivar) {
        return apple -> apple.getCultivar() == cultivar;
    }
}
