package sk.tuke.fei.kpi.oop.chapters.behavioral.strategy;

import java.util.Random;

public class PseudoRandomAppleGenerator implements AppleFactory.Generator {

    private final Random random = new Random();

    @Override
    public int generateWeight() {
        return random.nextInt(191) + 10;
    }

    @Override
    public Apple.Cultivar generateCultivar() {
        return Apple.Cultivar.values()[random.nextInt(Apple.Cultivar.values().length)];
    }

    @Override
    public Apple.QualityClass generateQualityClass() {
        return Apple.QualityClass.values()[random.nextInt(Apple.QualityClass.values().length)];
    }

    @Override
    public boolean isValid(Apple apple) {
        if (apple.getWeight() < 50 && apple.getQualityClass() == Apple.QualityClass.EXTRA) {
            return false;
        }

        if (apple.getCultivar() == Apple.Cultivar.MCINTOSH && apple.getQualityClass() == Apple.QualityClass.EXTRA) {
            return false;
        }

        return true;
    }
}
