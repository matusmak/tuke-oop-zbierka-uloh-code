package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.Set;

public class Article {

    private final String author;
    private final String title;
    private final String content;
    private final String slug;
    private final Set<String> categories;


    public Article(String author, String title, String content, String slug, Set<String> categories) {
        this.author = author;
        this.title = title;
        this.content = content;
        this.slug = slug;
        this.categories = categories;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getSlug() {
        return slug;
    }

    public Set<String> getCategories() {
        return categories;
    }
}
