package sk.tuke.fei.kpi.oop.chapters.behavioral.templateMethod;

public class ConsoleLogger extends AbstractLogger {

	@Override
	protected void write(String message) {
		System.out.println(message);
	}
}
