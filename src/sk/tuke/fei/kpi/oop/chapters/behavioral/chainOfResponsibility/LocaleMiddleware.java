package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.function.Function;

public class LocaleMiddleware implements Middleware {

	private static final String DEFAULT_LOCALE = "en";

	@Override
	public Response handle(Request request, Function<Request, Response> next) {
		String locale = request.getHeaders().getOrDefault("Locale", DEFAULT_LOCALE);
		request.setLocale(locale);

		return next.apply(request);
	}
}
