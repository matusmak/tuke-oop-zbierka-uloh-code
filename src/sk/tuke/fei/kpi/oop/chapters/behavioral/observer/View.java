package sk.tuke.fei.kpi.oop.chapters.behavioral.observer;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class View<V extends View> {

    private final View parent;
    private final String name;
    private Set<OnClickListener<V>> onClickListeners = new HashSet<>();

    public View(View parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    public View getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public void registerOnClickListener(OnClickListener<V> onClickListener) {
        onClickListeners.add(onClickListener);
    }

    public void unregisterOnClickListener(OnClickListener<V> onClickListener) {
        onClickListeners.remove(onClickListener);
    }

    public Set<OnClickListener<V>> getOnClickListeners() {
        return Collections.unmodifiableSet(onClickListeners);
    }
}
