package sk.tuke.fei.kpi.oop.chapters.behavioral.observer;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class ObservableList<E> implements List<E> {

    private final List<E> internalList;
    private final Set<Observer<E>> observers = new HashSet<>();

    public ObservableList(List<E> internalList) {
        this.internalList = Objects.requireNonNull(internalList);
    }

    public List<E> getInternalList() {
        return internalList;
    }

    public Set<Observer<E>> getObservers() {
        return Collections.unmodifiableSet(observers);
    }

    public void registerObserver(Observer<E> observer) {
        observers.add(Objects.requireNonNull(observer));
    }

    public void unregisterObserver(Observer<E> observer) {
        observers.remove(observer);
    }

    @Override
    public int size() {
        return internalList.size();
    }

    @Override
    public boolean isEmpty() {
        return internalList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return internalList.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return internalList.iterator();
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        internalList.forEach(action);
    }

    @Override
    public Object[] toArray() {
        return internalList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return internalList.toArray(a);
    }

    @Override
    public boolean add(E e) {
        boolean added = internalList.add(e);

        if (added) {
            observers.forEach(observer -> observer.onAdded(this, e));
        }

        return added;
    }

    @Override
    public boolean remove(Object o) {
        boolean removed = internalList.remove(o);

        if (removed) {
            observers.forEach(observer -> observer.onRemoved(this, o));
        }

        return removed;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return internalList.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean added = internalList.addAll(c);

        if (added) {
            observers.forEach(observer -> observer.onAddedAll(this, c));
        }

        return added;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        boolean added = internalList.addAll(index, c);

        if (added) {
            observers.forEach(observer -> observer.onAddedAll(this, c));
        }

        return added;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean removed = internalList.removeAll(c);

        if (removed) {
            observers.forEach(observer -> observer.onRemovedAll(this, c));
        }

        return removed;
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter) {
        return internalList.removeIf(filter);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean retained =  internalList.retainAll(c);

        if (retained) {
            observers.forEach(observer -> observer.onRetainedAll(this, c));
        }

        return retained;
    }

    @Override
    public void replaceAll(UnaryOperator<E> operator) {
        internalList.replaceAll(operator);
    }

    @Override
    public void sort(Comparator<? super E> c) {
        internalList.sort(c);
    }

    @Override
    public void clear() {
        internalList.clear();
        observers.forEach(observer -> observer.onCleared(this));
    }

    @Override
    public E get(int index) {
        return internalList.get(index);
    }

    @Override
    public E set(int index, E element) {
        E oldElement = internalList.set(index, element);

        observers.forEach(observer -> observer.onSet(this, index, oldElement, element));

        return oldElement;
    }

    @Override
    public void add(int index, E element) {
        internalList.add(index, element);
        observers.forEach(observer -> observer.onAdded(this, element));
    }

    @Override
    public E remove(int index) {
        E element = internalList.remove(index);

        observers.forEach(observer -> observer.onRemoved(this, element));

        return element;
    }

    @Override
    public int indexOf(Object o) {
        return internalList.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return internalList.lastIndexOf(o);
    }

    @Override
    public ListIterator<E> listIterator() {
        return internalList.listIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return internalList.listIterator(index);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return internalList.subList(fromIndex, toIndex);
    }

    @Override
    public Spliterator<E> spliterator() {
        return internalList.spliterator();
    }

    @Override
    public Stream<E> stream() {
        return internalList.stream();
    }

    @Override
    public Stream<E> parallelStream() {
        return internalList.parallelStream();
    }

    public interface Observer<T> {

        default void onAdded(ObservableList<T> list, T element) { }

        default void onRemoved(ObservableList<T> list, Object element) { }

        default void onAddedAll(ObservableList<T> list, Collection<? extends T> collection) { }

        default void onRemovedAll(ObservableList<T> list, Collection<?> collection) { }

        default void onRetainedAll(ObservableList<T> list, Collection<?> collection) { }

        default void onCleared(ObservableList<T> list) { }

        default void onSet(ObservableList<T> list, int index, T oldElement, T newElement) { }
    }
}
