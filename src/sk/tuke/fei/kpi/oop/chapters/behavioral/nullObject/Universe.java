package sk.tuke.fei.kpi.oop.chapters.behavioral.nullObject;

public class Universe {

	private final int size;
	private final Quantum[][] universe;

	public Universe(int size, Quantum[][] universe) {
		this.size = size;
		this.universe = universe;
	}

	public int getSize() {
		return size;
	}

	public Quantum[][] getUniverse() {
		return universe;
	}

	public Quantum getQuantum(int x, int y) {
		return universe[x][y];
	}
}
