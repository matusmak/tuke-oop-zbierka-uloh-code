package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

public class Response {

	private final String body;

	public Response(String body) {
		this.body = body;
	}

	public String getBody() {
		return body;
	}
}
