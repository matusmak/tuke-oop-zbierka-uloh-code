package sk.tuke.fei.kpi.oop.chapters.behavioral.chainOfResponsibility;

import java.util.function.Function;

public class AuthMiddleware implements Middleware {

	private static final String AUTH_TOKEN = "TKN345";
	private static final String IGNORED_URI_PREFIX = "auth/";

	@Override
	public Response handle(Request request, Function<Request, Response> next) {
		String authToken = request.getHeaders().get("Authenticate");

		if (!request.getUri().startsWith(IGNORED_URI_PREFIX) && !AUTH_TOKEN.equals(authToken)) {
			return new Response("Not authenticated!");
		}

		return next.apply(request);
	}
}
