package sk.tuke.fei.kpi.oop.chapters.behavioral.nullObject;

public class Quantum {

	private static final Quantum VACUUM = new Quantum(0, 0, State.UNKNOWN);

	private final int mass;
	private final int energy;
	private final State state;

	public Quantum(int mass, int energy, State state) {
		this.mass = mass;
		this.energy = energy;
		this.state = state;
	}

	public static Quantum vacuum() {
		return VACUUM;
	}

	public int getMass() {
		return mass;
	}

	public int getEnergy() {
		return energy;
	}

	public State getState() {
		return state;
	}

	public boolean isVacuum() {
		return mass == 0 && energy == 0 && state == State.UNKNOWN;
	}

	public enum State {
		SOLID,
		LIQUID,
		GAS,
		PLASMA,
		UNKNOWN
	}
}
