package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

public class PageController {

	public String getPage(Request request) {
		String language = request.getHeaders().get("Accept-Language");
		String title = "sk".equalsIgnoreCase(language) ? "Ahojte!" : "Hello!";

		return String.format("<h1>%s</h1>", title);
	}
}
