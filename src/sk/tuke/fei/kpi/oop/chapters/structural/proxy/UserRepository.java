package sk.tuke.fei.kpi.oop.chapters.structural.proxy;

import java.util.Optional;

public interface UserRepository {

	Optional<User> getUser(long id);

	void setUser(User user);

	void removeUser(long id);
}
