package sk.tuke.fei.kpi.oop.chapters.structural.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class MemoryUserRepository implements UserRepository {

	private Map<Long, User> users = new HashMap<>();

	@Override
	public Optional<User> getUser(long id) {
		System.out.println("Accessing user repository...");
		return Optional.ofNullable(users.get(id));
	}

	@Override
	public void setUser(User user) {
		Objects.requireNonNull(user);
		users.put(user.getId(), user);
	}

	@Override
	public void removeUser(long id) {
		users.remove(id);
	}
}
