package sk.tuke.fei.kpi.oop.chapters.structural.decorator;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MySQLDatabaseConnector implements DatabaseConnector {

	private final Random random = new Random();

	@Override
	public void connect() {
		randomSleep();
		randomSleep();
		randomSleep();
		randomSleep();
	}

	@Override
	public void execute(String query) {
		randomSleep();
	}

	private void randomSleep() {
		int milliseconds = random.nextInt(1000);
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
