package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.util.Objects;

public class Age implements Serializable, Comparable<Age> {

	private final int age;

	public Age(int age) {
		if (age < 0) {
			throw new IllegalArgumentException("Age must be a natural number or zero!");
		}

		this.age = age;
	}

	public int value() {
		return age;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Age other = (Age) o;
		return age == other.age;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), age);
	}

	@Override
	public String toString() {
		return "Age(" + age + ")";
	}

	@Override
	public int compareTo(Age other) {
		Objects.requireNonNull(other, "Can't compare to null");
		return Integer.compare(age, other.age);
	}
}
