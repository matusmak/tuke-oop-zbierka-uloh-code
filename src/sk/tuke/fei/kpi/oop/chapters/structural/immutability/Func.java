package sk.tuke.fei.kpi.oop.chapters.structural.immutability;

import java.util.Objects;
import java.util.function.Function;

public class Func {

	public static final Func ZERO = new Func(x -> 0d);
	public static final Func LINEAR = new Func(x -> x);
	public static final Func QUADRATIC = new Func(x -> x * x);
	public static final Func CUBIC = new Func(x -> x * x * x);
	public static final Func SQUARE_ROOT = new Func(Math::sqrt);
	public static final Func EXPONENTIAL = new Func(Math::exp);
	public static final Func SIN = new Func(Math::sin);
	public static final Func COS = new Func(Math::cos);
	public static final Func TAN = new Func(Math::tan);

	private final Function<Double, Double> function;

	public Func(Function<Double, Double> function) {
		this.function = Objects.requireNonNull(function);
	}

	public double apply(double x) {
		return function.apply(x);
	}

	public Func add(Func other) {
		return new Func(x -> apply(x) + other.apply(x));
	}

	public Func add(Function<Double, Double> function) {
		return add(new Func(function));
	}

	public Func add(double constant) {
		return new Func(x -> apply(x) + constant);
	}

	public Func sub(Func other) {
		return new Func(x -> apply(x) - other.apply(x));
	}

	public Func sub(Function<Double, Double> function) {
		return sub(new Func(function));
	}

	public Func sub(double constant) {
		return new Func(x -> apply(x) - constant);
	}

	public Func mul(Func other) {
		return new Func(x -> apply(x) * other.apply(x));
	}

	public Func mul(Function<Double, Double> function) {
		return mul(new Func(function));
	}

	public Func mul(double constant) {
		return new Func(x -> apply(x) * constant);
	}

	public Func div(Func other) {
		return new Func(x -> apply(x) / other.apply(x));
	}

	public Func div(Function<Double, Double> function) {
		return div(new Func(function));
	}

	public Func div(double constant) {
		if (constant == 0) {
			throw new IllegalArgumentException("Cannot divide by zero!");
		}
		return new Func(x -> apply(x) / constant);
	}

	public Func compose(Func other) {
		return new Func(x -> apply(other.apply(x)));
	}

	public Func compose(Function<Double, Double> function) {
		return compose(new Func(function));
	}

	public Func negate() {
		return new Func(x -> -apply(x));
	}
}
