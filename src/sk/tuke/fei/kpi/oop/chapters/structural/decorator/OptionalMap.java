package sk.tuke.fei.kpi.oop.chapters.structural.decorator;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class OptionalMap<K, V> implements Map<K, V> {

	private final Map<K, V> map;

	public OptionalMap(Map<K, V> map) {
		this.map = Objects.requireNonNull(map);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public V get(Object key) {
		return map.get(key);
	}

	public Optional<V> getOptional(Object key) {
		return Optional.ofNullable(get(key));
	}

	@Override
	public V put(K key, V value) {
		return map.put(key, value);
	}

	public Optional<V> putOptional(K key, V value) {
		return Optional.ofNullable(put(key, value));
	}

	@Override
	public V remove(Object key) {
		return map.remove(key);
	}

	public Optional<V> removeOptional(Object key) {
		return Optional.ofNullable(remove(key));
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		map.putAll(m);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<K> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<V> values() {
		return map.values();
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return map.entrySet();
	}

	@Override
	public V getOrDefault(Object key, V defaultValue) {
		return map.get(key);
	}

	public Optional<V> getOrDefaultOptional(Object key, V defaultValue) {
		return Optional.ofNullable(getOrDefault(key, defaultValue));
	}

	@Override
	public void forEach(BiConsumer<? super K, ? super V> action) {
		map.forEach(action);
	}

	@Override
	public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
		map.replaceAll(function);
	}

	@Override
	public V putIfAbsent(K key, V value) {
		return map.putIfAbsent(key, value);
	}

	public Optional<V> putIfAbsentOptional(K key, V value) {
		return Optional.ofNullable(putIfAbsent(key, value));
	}

	@Override
	public boolean remove(Object key, Object value) {
		return map.remove(key, value);
	}

	@Override
	public boolean replace(K key, V oldValue, V newValue) {
		return map.replace(key, oldValue, newValue);
	}

	@Override
	public V replace(K key, V value) {
		return map.replace(key, value);
	}

	public Optional<V> replaceOptional(K key, V value) {
		return Optional.ofNullable(replace(key, value));
	}

	@Override
	public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
		return map.computeIfAbsent(key, mappingFunction);
	}

	public Optional<V> computeIfAbsentOptional(K key, Function<? super K, ? extends V> mappingFunction) {
		return Optional.ofNullable(computeIfAbsent(key, mappingFunction));
	}

	@Override
	public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return map.computeIfPresent(key, remappingFunction);
	}

	public Optional<V> computeIfPresentOptional(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return Optional.ofNullable(computeIfPresent(key, remappingFunction));
	}

	@Override
	public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return map.compute(key, remappingFunction);
	}

	public Optional<V> computeOptional(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return Optional.ofNullable(compute(key, remappingFunction));
	}

	@Override
	public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
		return map.merge(key, value, remappingFunction);
	}

	public Optional<V> mergeOptional(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
		return Optional.ofNullable(merge(key, value, remappingFunction));
	}
}
