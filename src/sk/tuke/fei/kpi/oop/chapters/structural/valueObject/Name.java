package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class Name implements Serializable, Comparable<Name> {

	private final String titlesBeforeName;
	private final String forename;
	private final String surname;
	private final String titlesAfterName;

	public Name(String titlesBeforeName, String forename, String surname, String titlesAfterName) {
		this.titlesBeforeName = titlesBeforeName;
		this.forename = Objects.requireNonNull(forename, "Forename cannot be null!");
		this.surname = Objects.requireNonNull(surname, "Surname cannot be null!");
		this.titlesAfterName = titlesAfterName;
	}

	public Name(String titlesBeforeName, String forename, String surname) {
		this(titlesBeforeName, forename, surname, null);
	}

	public Name(String forename, String surname) {
		this(null, forename, surname, null);
	}

	public String value() {
		StringBuilder builder = new StringBuilder();

		getTitlesBeforeName().ifPresent(titles -> builder.append(titles).append(' '));
		builder.append(forename).append(' ').append(surname);
		getTitlesAfterName().ifPresent(titles -> builder.append(", ").append(titles));

		return builder.toString();
	}

	public Optional<String> getTitlesBeforeName() {
		return Optional.ofNullable(titlesBeforeName);
	}

	public String getForename() {
		return forename;
	}

	public String getSurname() {
		return surname;
	}

	public Optional<String> getTitlesAfterName() {
		return Optional.ofNullable(titlesAfterName);
	}

	public String getFullName() {
		return forename + ' ' + surname;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Name other = (Name) o;
		return Objects.equals(titlesBeforeName, other.titlesBeforeName) &&
			forename.equals(other.forename) &&
			surname.equals(other.surname) &&
			Objects.equals(titlesAfterName, other.titlesAfterName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), titlesBeforeName, forename, surname, titlesAfterName);
	}

	@Override
	public String toString() {
		return "Name(" + value() + ")";
	}

	@Override
	public int compareTo(Name other) {
		Objects.requireNonNull(other, "Cannot compare to null!");
		return getFullName().compareTo(other.getFullName());
	}
}
