package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

import java.util.Arrays;

public class SearchContext {

	private static final String[] ORDERING_FIELDS = {
			"name", "surname", "age"
	};

	private String name = null;
	private String surname = null;
	private int ageFrom = Integer.MIN_VALUE;
	private int ageTo = Integer.MAX_VALUE;
	private String orderBy = null;
	private boolean ascending = true;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name != null ? name.trim() : null;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname != null ? surname.trim() : null;
	}

	public int getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(int ageFrom) {
		this.ageFrom = ageFrom;
	}

	public int getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(int ageTo) {
		this.ageTo = ageTo;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		// Chances are you would probably want to store
		// such a value using enum, but we wanted to
		// show how POJO can help you validate data
		if (orderBy != null && Arrays.stream(ORDERING_FIELDS).noneMatch(orderBy::equals)) {
			throw new IllegalArgumentException("Invalid ordering field!");
		}
		this.orderBy = orderBy;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}
}
