package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

import java.util.Objects;

public class Point {

	protected final int x;
	protected final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object o) {
		// Is given object the same instance as this?
		if (this == o) {
			return true;
		}

		// Is given object instance of the same class?
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		// Are other Point's attributes the same?
		Point point = (Point) o;
		return x == point.x &&
				y == point.y;
	}

	@Override
	public int hashCode() {
		// By adding Class object into the hash code
		// generation, we are ensuring that subclasses
		// (in our case Vector) won't have the same hash
		return Objects.hash(getClass(), x, y);
	}

	@Override
	public String toString() {
		return "(" + x + "; " + y + ")";
	}
}
