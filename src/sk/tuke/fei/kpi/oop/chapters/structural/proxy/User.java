package sk.tuke.fei.kpi.oop.chapters.structural.proxy;

import java.util.Objects;
import java.util.Optional;

public class User {

	private final long id;
	private final String name;
	private final String surname;
	private final String bio;

	public User(long id, String name, String surname, String bio) {
		this.id = id;
		this.name = Objects.requireNonNull(name);
		this.surname = Objects.requireNonNull(surname);
		this.bio = bio;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Optional<String> getBio() {
		return Optional.ofNullable(bio);
	}

	@Override
	public String toString() {
		String bioText = getBio().orElse("Sorry, user's bio is empty.");
		return "User(" + id + ", " + name + " " + surname + ", " + bioText + ")";
	}
}
