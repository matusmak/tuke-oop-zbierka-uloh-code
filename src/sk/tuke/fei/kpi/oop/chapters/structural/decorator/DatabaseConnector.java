package sk.tuke.fei.kpi.oop.chapters.structural.decorator;

public interface DatabaseConnector {

	void connect();

	void execute(String query);
}
