package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.util.Objects;

public class Address implements Serializable, Comparable<Address> {

	private final String streetName;
	private final String streetNumber;
	private final String zip;
	private final String city;
	private final String country;

	public Address(String streetName, String streetNumber, String zip, String city, String country) {
		// For simplicity, we will only check for null values,
		// not if those values are properly formatted
		this.streetName = Objects.requireNonNull(streetName, "Street name cannot be null!");
		this.streetNumber = Objects.requireNonNull(streetNumber, "Street number cannot be null!");
		this.zip = Objects.requireNonNull(zip, "ZIP cannot be null!");
		this.city = Objects.requireNonNull(city, "City cannot be null!");
		this.country = Objects.requireNonNull(country, "Country cannot be null!");
	}

	public String value() {
		return streetName + " " + streetNumber +
				", " +
				zip + " " + city +
				", " +
				country;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public String getZip() {
		return zip;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Address other = (Address) o;
		return streetName.equals(other.streetName) &&
			streetNumber.equals(other.streetNumber) &&
			zip.equals(other.zip) &&
			city.equals(other.city) &&
			country.equals(other.country);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), streetName, streetNumber, zip, city, country);
	}

	@Override
	public String toString() {
		return "Address(" + value() + ")";
	}

	@Override
	public int compareTo(Address other) {
		Objects.requireNonNull(other, "Cannot compare to null!");
		return value().compareTo(other.value());
	}
}
