package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Request {

	private final Method method;
	private final String uri;
	private final String protocolVersion;
	private final Map<String, String> headers;
	private final String body;

	// As cookies, session and user information
	// would be another complex objects, we are
	// leaving them out from this example

	public Request(Method method, String uri, String protocolVersion, Map<String, String> headers, String body) {
		this.method = Objects.requireNonNull(method);
		this.uri = Objects.requireNonNullElse(uri, "/");
		this.protocolVersion = Objects.requireNonNullElse(protocolVersion, "1.1");
		this.headers = Objects.requireNonNullElse(headers, new HashMap<>());
		this.body = body;
	}

	public Method getMethod() {
		return method;
	}

	public String getUri() {
		return uri;
	}

	public String getProtocolVersion() {
		return protocolVersion;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getBody() {
		return body;
	}

	public enum Method {
		GET, POST, PUT, DELETE, PATCH,
		HEAD, TRACE, OPTIONS, CONNECT
	}
}
