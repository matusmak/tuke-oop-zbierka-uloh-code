package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

import java.util.Objects;

public class Circle implements Comparable<Circle> {

	private final Point origin;
	private final int radius;

	private double cachedCircumference = Double.NaN;
	private double cachedDiskArea = Double.NaN;

	public Circle(Point origin, int radius) {
		this.origin = origin;
		this.radius = radius;
	}

	public Circle(int originX, int originY, int radius) {
		this(new Point(originX, originY), radius);
	}

	public Point getOrigin() {
		return origin;
	}

	public int getRadius() {
		return radius;
	}

	public int getDiameter() {
		return radius * 2;
	}

	public double getCircumference() {
		if (Double.isNaN(cachedCircumference)) {
			cachedCircumference = 2 * Math.PI * radius;
		}

		return cachedCircumference;
	}

	public double getDiskArea() {
		if (Double.isNaN(cachedDiskArea)) {
			cachedDiskArea = Math.PI * Math.pow(radius, 2);
		}

		return cachedDiskArea;
	}

	@Override
	public boolean equals(Object o) {
		// Is given object the same instance as this?
		if (this == o) {
			return true;
		}

		// Is given object instance of the same class?
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		// Are other Circle's attributes the same?
		Circle circle = (Circle) o;
		return radius == circle.radius &&
				Objects.equals(origin, circle.origin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(origin, radius);
	}

	@Override
	public String toString() {
		// We are using brackets instead of parentheses
		// for better readability as parentheses are
		// already used by origin, which is Point
		return "[" + origin + ", " + radius + "]";
	}

	@Override
	public int compareTo(Circle other) {
		return Integer.compare(radius, other.radius);
	}
}
