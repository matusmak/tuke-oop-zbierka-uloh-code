package sk.tuke.fei.kpi.oop.chapters.structural.pojo;

public class Vector extends Point {

	public static final Vector ZERO = new Vector(0, 0);

	private double cachedMagnitude = Double.NaN;

	public Vector(int x, int y) {
		super(x, y);
	}

	public Vector(Point from, Point to) {
		this(to.x - from.x, to.y - from.y);
	}

	public double getMagnitude() {
		if (cachedMagnitude == Double.NaN) {
			cachedMagnitude = distance(ZERO);
		}

		return cachedMagnitude;
	}

	public Vector add(Vector other) {
		return new Vector(x + other.x, y + other.y);
	}

	public Vector sub(Vector other) {
		return new Vector(x - other.x, y - other.y);
	}

	public Vector mul(int scalar) {
		return new Vector(x * scalar, y * scalar);
	}

	public int dot(Vector other) {
		return x * other.x + y * other.y;
	}

	public Vector negate() {
		return new Vector(-x, -y);
	}

	public double distance(Vector other) {
		return Math.hypot(x - other.x, y - other.y);
	}

	@Override
	public boolean equals(Object o) {
		// Is given object the same instance as this?
		if (this == o) {
			return true;
		}

		// Is given object instance of the same class?
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		// Are other Vector's attributes the same?
		Vector vector = (Vector) o;
		return x == vector.x &&
				y == vector.y;
	}
}
