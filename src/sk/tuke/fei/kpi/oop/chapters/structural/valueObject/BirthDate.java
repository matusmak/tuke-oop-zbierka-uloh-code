package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class BirthDate implements Serializable, Comparable<BirthDate> {

	private final LocalDate birthDate;

	public BirthDate(LocalDate birthDate) {
		this.birthDate = Objects.requireNonNull(birthDate, "Birth date cannot be null!");
	}

	public LocalDate value() {
		return birthDate;
	}

	public boolean isInPast() {
		return !LocalDate.now().isBefore(birthDate);
	}

	public int getAge() {
		int age = birthDate.until(LocalDate.now()).getYears();

		if (age < 0) {
			throw new IllegalStateException("Birth date is in the future");
		}

		return age;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BirthDate other = (BirthDate) o;
		return birthDate.equals(other.birthDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), birthDate);
	}

	@Override
	public int compareTo(BirthDate other) {
		Objects.requireNonNull(other, "Cannot compare to null!");
		return birthDate.compareTo(other.birthDate);
	}
}
