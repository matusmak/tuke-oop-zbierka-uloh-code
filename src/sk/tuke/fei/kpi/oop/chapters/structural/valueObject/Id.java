package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Id implements Serializable, Comparable<Id> {

	private final String reversedBirthDate;
	private final String suffix;

	public Id(String id) {
		verifyID(id);
		String[] parts = id.split("/", 2);
		this.reversedBirthDate = parts[0];
		this.suffix = parts[1];
	}

	public Id(String reversedBirthDate, String suffix) {
		this(reversedBirthDate + "/" + suffix);
	}

	public String value() {
		return reversedBirthDate + "/" + suffix;
	}

	public String getReversedBirthDate() {
		return reversedBirthDate;
	}

	public String getSuffix() {
		return suffix;
	}

	public BirthDate toBirthDate() {
		int year = Integer.parseInt(reversedBirthDate.substring(0, 2));
		int month = Integer.parseInt(reversedBirthDate.substring(2, 4));
		int day = Integer.parseInt(reversedBirthDate.substring(4, 6));

		LocalDate birthDate = LocalDate.of(
			year,
			month > 50 ? month - 50 : month,
			day
		);
		return new BirthDate(birthDate);
	}

	public Sex toSex() {
		int month = Integer.parseInt(reversedBirthDate.substring(2, 4));
		return new Sex(month < 51);
	}

	private void verifyID(String raw) {
		// Verifying Slovak ID is a bit more complex than this,
		// but for the sake of demonstration, we will only check if
		// the whole number is divisible by 11.
		String cleaned = raw.replace("/", "");
		long parsed = Long.parseLong(cleaned);
		if (parsed % 11 != 0) {
			throw new IllegalArgumentException("ID must be divisible by 11!");
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Id other = (Id) o;
		return reversedBirthDate.equals(other.reversedBirthDate) &&
			suffix.equals(other.suffix);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), reversedBirthDate, suffix);
	}

	@Override
	public String toString() {
		return "Id(" + value() + ")";
	}

	@Override
	public int compareTo(Id other) {
		Objects.requireNonNull(other, "Cannot compare to null!");
		return value().compareTo(other.value());
	}
}
