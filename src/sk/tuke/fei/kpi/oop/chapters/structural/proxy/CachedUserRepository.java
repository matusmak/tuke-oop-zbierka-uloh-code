package sk.tuke.fei.kpi.oop.chapters.structural.proxy;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.Comparator.comparing;

public class CachedUserRepository implements UserRepository {

	private static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(3);
	private static final int DEFAULT_MAX_CACHE_SIZE = 10;

	private final UserRepository repository;
	private final Duration timeout;
	private final int maxCacheSize;
	private final Map<Long, UserCache> cachedUsers;

	public CachedUserRepository(UserRepository repository) {
		this(repository, DEFAULT_TIMEOUT, DEFAULT_MAX_CACHE_SIZE);
	}

	public CachedUserRepository(UserRepository repository, Duration timeout, int maxCacheSize) {
		this.repository = repository;
		this.timeout = validatedTimeout(timeout);
		this.maxCacheSize = validatedMaxCacheSize(maxCacheSize);
		this.cachedUsers = new HashMap<>(maxCacheSize);
	}

	private Duration validatedTimeout(Duration timeout) {
		Objects.requireNonNull(timeout);
		if (timeout.isZero() || timeout.isNegative()) {
			throw new IllegalArgumentException("Timeout must be a positive duration!");
		}
		return timeout;
	}

	private int validatedMaxCacheSize(int maxCacheSize) {
		if (maxCacheSize <= 0) {
			throw new IllegalArgumentException("Maximum cache size must be a positive number!");
		}
		return maxCacheSize;
	}

	@Override
	public Optional<User> getUser(long id) {
		// Fetch user's cache. Might not exist.
		UserCache existingCache = cachedUsers.get(id);

		// Check if cache is expired or non existing.
		// If so, make sure it's removed from cache and
		// if user of specified ID exists, cache it again
		if (existingCache == null || isExpired(existingCache)) {
			cachedUsers.remove(id);
			Optional<User> userFromRepository = repository.getUser(id);
			userFromRepository.ifPresent(user -> {
					freeCacheIfNecessary();
					cachedUsers.put(id, new UserCache(user, LocalDateTime.now().plus(timeout)));
				}
			);
		}

		// To simplify code, we are refetching any existing cache one more time.
		// If user exists, it is guaranteed that it will be returned here.
		return Optional.ofNullable(cachedUsers.get(id))
			.map(UserCache::getUser);
	}

	@Override
	public void setUser(User user) {
		repository.setUser(user);
	}

	@Override
	public void removeUser(long id) {
		repository.removeUser(id);
		cachedUsers.remove(id);
	}

	private boolean isExpired(UserCache cache) {
		return LocalDateTime.now().isAfter(cache.getExpirationTime());
	}

	private void freeCacheIfNecessary() {
		if (cachedUsers.size() >= maxCacheSize) {
			cachedUsers.values().stream()
				.min(comparing(UserCache::getExpirationTime))
				.ifPresent(cache -> cachedUsers.remove(cache.getUser().getId()));
		}
	}

	private class UserCache {

		private final User user;
		private final LocalDateTime expirationTime;

		private UserCache(User user, LocalDateTime expirationTime) {
			this.user = user;
			this.expirationTime = expirationTime;
		}

		public User getUser() {
			return user;
		}

		public LocalDateTime getExpirationTime() {
			return expirationTime;
		}
	}
}
