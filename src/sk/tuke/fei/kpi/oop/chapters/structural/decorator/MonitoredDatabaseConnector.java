package sk.tuke.fei.kpi.oop.chapters.structural.decorator;

import java.text.DecimalFormat;
import java.util.Objects;

public class MonitoredDatabaseConnector implements DatabaseConnector {

	private final DatabaseConnector databaseConnector;
	private final DecimalFormat formatter = new DecimalFormat("0.0#####");

	public MonitoredDatabaseConnector(DatabaseConnector databaseConnector) {
		this.databaseConnector = Objects.requireNonNull(databaseConnector);
	}

	@Override
	public void connect() {
		System.out.println("Connecting to database...");
		String connectingTime = watch(databaseConnector::connect);
		System.out.println("Connected! Total time: " + connectingTime + " milliseconds.");
	}

	@Override
	public void execute(String query) {
		System.out.println("Executing query: " + query);
		String executionTime = watch(() -> databaseConnector.execute(query));
		System.out.println("Executed! Total time: " + executionTime + " milliseconds.");
	}

	private String watch(Runnable runnable) {
		long start = System.nanoTime();
		runnable.run();
		long end = System.nanoTime();

		return formatter.format((end - start) / 1000000D);
	}
}
