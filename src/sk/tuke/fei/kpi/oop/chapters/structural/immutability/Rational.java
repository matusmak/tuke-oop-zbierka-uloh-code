package sk.tuke.fei.kpi.oop.chapters.structural.immutability;

public class Rational {

	public static final Rational ZERO = new Rational(0);
	public static final Rational ONE = new Rational(1);
	public static final Rational NEG_ONE = new Rational(-1);

	private final int numerator;
	private final int denominator;

	public Rational(int numerator, int denominator) {
		if (denominator < 1) {
			throw new IllegalArgumentException("Denominator must be a natural number!");
		}

		int gcd = calculateGCD(Math.abs(numerator), denominator);
		this.numerator = numerator / gcd;
		this.denominator = denominator / gcd;
	}

	public Rational(int number) {
		this(number, 1);
	}

	public int getNumerator() {
		return numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public double toDouble() {
		return numerator / (denominator * 1.0d);
	}

	public Rational negate() {
		return new Rational(-numerator, denominator);
	}

	public Rational add(Rational other) {
		return new Rational(
				numerator * other.denominator + other.numerator * denominator,
				denominator * other.denominator
		);
	}

	public Rational add(int number) {
		return add(new Rational(number));
	}

	public Rational sub(Rational other) {
		return add(other.negate());
	}

	public Rational sub(int other) {
		return add(-other);
	}

	public Rational mul(Rational other) {
		return new Rational(
				numerator * other.numerator,
				denominator * other.denominator
		);
	}

	public Rational mul(int other) {
		return mul(new Rational(other));
	}

	public Rational div(Rational other) {
		if (other.numerator == 0) {
			throw new IllegalArgumentException("Cannot divide by zero!");
		}

		return mul(new Rational(other.denominator, other.numerator));
	}

	public Rational div(int other) {
		if (other == 0) {
			throw new IllegalArgumentException("Cannot divide by zero!");
		}

		return mul(new Rational(1, other));
	}

	@Override
	public String toString() {
		return numerator + " / " + denominator;
	}

	@Override
	public boolean equals(Object obj) {
		// Check if given object is an instance of this class
		if (!(obj instanceof Rational)) {
			return false;
		}

		// Compare numerator and denominator of both rationals
		Rational other = (Rational) obj;
		return denominator == other.denominator
			&& numerator == other.numerator;
	}

	private int calculateGCD(int a, int b) {
		int dividend = Math.max(a, b);
		int divisor = Math.min(a, b);

		while (divisor > 0) {
			int remainder = dividend % divisor;
			dividend = divisor;
			divisor = remainder;
		}

		return dividend;
	}
}
