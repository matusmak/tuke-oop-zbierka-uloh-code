package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class Sex implements Serializable, Comparable<Sex> {

	/**
	 * We are using boxed boolean to allow for null value
	 * and therefore have 3 different states:
	 * - null = unspecified
	 * - false = female
	 * - true = male
	 */
	private final Boolean isMale;

	public Sex(Boolean isMale) {
		this.isMale = isMale;
	}

	public boolean isUnspecified() {
		return isMale == null;
	}

	public boolean isMale() {
		return !isUnspecified() && isMale;
	}

	public boolean isFemale() {
		return !isUnspecified() && !isMale;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Sex other = (Sex) o;
		return Objects.equals(isMale, other.isMale);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), isMale);
	}

	@Override
	public String toString() {
		String sexName = Optional.ofNullable(isMale)
			.map(male -> male ? "male" : "female")
			.orElse("unspecified");

		return "Sex(" + sexName + ")";
	}

	@Override
	public int compareTo(Sex other) {
		Objects.requireNonNull(other, "Cannot compare to null!");

		if (isUnspecified() && other.isUnspecified()) {
			return 0;
		}
		return Boolean.compare(isMale, other.isMale);
	}
}
