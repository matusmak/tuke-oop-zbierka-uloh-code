package sk.tuke.fei.kpi.oop.chapters.structural.immutability;

/**
 * We called this interface version of {@link Func} `Fun`,
 * because they are in the same namespace and we needed to
 * distinguish between them. This name is okay, as a lot of
 * languages are referring to `function` as `fun` for short.
 */
@FunctionalInterface
public interface Fun {

	double apply(double x);

	default Fun add(Fun other) {
		return x -> apply(x) + other.apply(x);
	}

	default Fun add(double constant) {
		return x -> apply(x) + constant;
	}

	default Fun sub(Fun other) {
		return x -> apply(x) - other.apply(x);
	}

	default Fun sub(double constant) {
		return x -> apply(x) - constant;
	}

	default Fun mul(Fun other) {
		return x -> apply(x) * other.apply(x);
	}

	default Fun mul(double constant) {
		return x -> apply(x) * constant;
	}

	default Fun div(Fun other) {
		return x -> apply(x) / other.apply(x);
	}

	default Fun div(double constant) {
		if (constant == 0) {
			throw new IllegalArgumentException("Cannot divide by zero!");
		}
		return x -> apply(x) / constant;
	}

	default Fun compose(Fun other) {
		return x -> apply(other.apply(x));
	}

	default Fun negate() {
		return x -> -apply(x);
	}

	static Fun zero() {
		return x -> 0d;
	}

	static Fun linear() {
		return x -> x;
	}

	static Fun quadratic() {
		return x -> x * x;
	}

	static Fun cubic() {
		return x -> x * x * x;
	}

	static Fun squareRoot() {
		return Math::sqrt;
	}

	static Fun exponential() {
		return Math::exp;
	}

	static Fun sin() {
		return Math::sin;
	}

	static Fun cos() {
		return Math::cos;
	}

	static Fun tan() {
		return Math::tan;
	}
}
