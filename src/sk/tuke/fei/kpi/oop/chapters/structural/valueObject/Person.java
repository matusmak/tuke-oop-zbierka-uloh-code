package sk.tuke.fei.kpi.oop.chapters.structural.valueObject;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable, Comparable<Person> {

	private final Id id;
	private final Name name;
	private final BirthDate birthDate;
	private final Sex sex;
	private final Address address;

	public Person(Id id, Name name, BirthDate birthDate, Sex sex, Address address) {
		this.id = Objects.requireNonNull(id, "Id cannot be null!");
		this.name = Objects.requireNonNull(name, "Name cannot be null!");
		this.birthDate = Objects.requireNonNull(birthDate, "Birth date cannot be null!");
		this.sex = Objects.requireNonNull(sex, "Sex cannot be null!");
		this.address = Objects.requireNonNull(address, "Address cannot be null!");
	}

	public Id getId() {
		return id;
	}

	public Name getName() {
		return name;
	}

	public BirthDate getBirthDate() {
		return birthDate;
	}

	public Sex getSex() {
		return sex;
	}

	public Address getAddress() {
		return address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Person other = (Person) o;
		return id.equals(other.id) &&
			name.equals(other.name) &&
			birthDate.equals(other.birthDate) &&
			sex.equals(other.sex) &&
			address.equals(other.address);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getClass(), id, name, birthDate, sex, address);
	}

	@Override
	public String toString() {
		return "Person(" +
			 	   id +
			", " + name +
			", " + birthDate +
			", " + sex +
			", " + address +
			')';
	}

	@Override
	public int compareTo(Person other) {
		Objects.requireNonNull(other, "Cannot compare to null!");
		return name.compareTo(other.name);
	}
}
