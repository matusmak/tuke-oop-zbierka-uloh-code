package sk.tuke.fei.kpi.oop.chapters.preparations;

public class Car {

	private final String brand;
	private final String model;
	private final int modelYear;

	public Car(String brand, String model, int modelYear) {
		this.brand = brand;
		this.model = model;
		this.modelYear = modelYear;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public int getModelYear() {
		return modelYear;
	}

	public void honk() {
		System.out.println("Beep Beeeeeep!");
	}
}
